import sys
import pickle
import sqlite3
import os
from ftfy import fix_text
from requests import Session
from getpass import getpass
from urllib.parse import urljoin, urlparse, unquote
from lxml import html
from pathlib import Path
from time import sleep
from html import unescape


DELAY = 1


SQL_CREATE_TABLE = """
CREATE TABLE IF NOT EXISTS files (
    link TEXT PRIMARY KEY NOT NULL,
    etag TEXT NOT NULL
);
"""

SQL_ADD_FILE = """
INSERT OR REPLACE INTO files VALUES (?,?);
"""

SQL_GET_FILE = """
SELECT etag FROM files WHERE link=?;
"""


def login(url):
    print('User not logged in, checking IdP')
    resp = session.get(url)
    tree = html.fromstring(resp.text)
    if tree.find('.//title') is not None and \
            "TUNI Web Login" in tree.find('.//title').text:
        print('Login needed!')
        login_form = tree.forms[0]
        post_url = login_form.get('action')
        username = input('Username: ')
        password = getpass('Password: ')
        post_data = {'j_username': username,
                     'j_password': password,
                     '_eventId_proceed': ''}
        resp = session.post(urljoin('https://idp.tuni.fi/', post_url),
                            post_data, allow_redirects=True)
        if 'was incorrect.' in resp.text:
            print('Incorrect username or password! Try again!')
            return login(url)
        elif 'stale request' in resp.text.lower():
            print('Stale request! Try again!')
            return login(url)
        tree = html.fromstring(resp.text)

    continue_form = tree.forms[0]
    relay_state = continue_form.fields['RelayState']
    saml_resp = continue_form.fields['SAMLResponse']
    post_url = continue_form.get('action')
    post_data = {'RelayState': relay_state, 'SAMLResponse': saml_resp}
    resp = session.post(post_url, post_data, allow_redirects=True)
    return resp


def download_file(url):
    if url.endswith('.pdf'):
        print(f'Checking file {url}...')
        sleep(DELAY)
        resp = session.get(url, stream=True)
        if 'ETag' not in resp.headers:
            return

        server_etag = resp.headers.get('ETag')
        db_etag = cursor.execute(SQL_GET_FILE, (url,)).fetchone()
        if db_etag is not None:
            db_etag = db_etag[0]
        if db_etag is None or server_etag != db_etag:
            print(f'Downloading!')
            fname = unquote(unescape(os.path.basename(urlparse(url).path)))

            fname = fix_text(fname)
            print(f'Fixed filename: {fname}')
            filename, extension = os.path.splitext(fname)

            files = os.listdir(os.getcwd())

            num = 1
            while fname in files:
                fname = filename + '(' + str(num) + ')' + extension
                num += 1

            print(f'Final filename: {fname}')
            with open(fname, 'wb') as out_file:
                out_file.write(resp.content)

            cursor.execute(SQL_ADD_FILE, (url, server_etag))
            connection.commit()
        else:
            print('ETag matches the one in the database!')
        resp.close()


def download_section(url):
    print(f'Entering {url}...')
    resp = session.get(url)
    tree = html.fromstring(resp.text)
    iframe_srcs = tree.xpath('.//iframe/@src')
    links = tree.xpath('.//a/@href')
    for iframe_s in iframe_srcs:
        download_file(iframe_s)
    for link in links:
        download_file(urljoin('https://plus.tuni.fi/', link))


def download_course(url):
    resp = session.get(url, allow_redirects=True)
    tree = html.fromstring(resp.text)
    if 'Kirjaudu sisään' in tree.find('.//title').text or 'Login' in tree.find(
            './/title').text:
        login_link = tree.cssselect('.site-content .row '
                                    '.col-sm-6:first-child a')[0].get('href')
        resp = login(urljoin('https://plus.tuni.fi/', login_link))
        tree = html.fromstring(resp.text)
    panels = tree.cssselect('.panel')
    for panel in panels:
        for link in panel.xpath('.//table//a'):
            href = link.get('href')
            if '#' not in href and 'submissions' not in href:
                sleep(DELAY)
                download_section(urljoin('https://plus.tuni.fi/', href))


if __name__ == '__main__':
    url_file = Path('course.txt')
    if not url_file.is_file():
        if len(sys.argv) < 2:
            print('Not enough arguments, '
                  'first run needs course url as argument!')
            exit(1)
        else:
            main_url = sys.argv[1]
            with open(url_file, 'w') as u_file:
                u_file.write(main_url)
    else:
        with open(url_file, 'r') as u_file:
            main_url = u_file.readline()

    script_dir = sys.path[0]
    session = Session()

    cookie_save_path = Path(script_dir, 'cookies.bin')
    if cookie_save_path.is_file():
        with open(cookie_save_path, 'rb') as cookie_file:
            print('Loading cookies!')
            session.cookies.update(pickle.load(cookie_file))

    connection = sqlite3.connect('files.db')
    cursor = connection.cursor()
    cursor.execute(SQL_CREATE_TABLE)
    connection.commit()

    session.headers = {'User-Agent': 'plus-downloader',
                       'Accept-Language':
                           'fi-FI,fi;q=0.8,en-US;q=0.5,en;q=0.3'}
    download_course(main_url)

    connection.commit()
    connection.close()

    print('Saving cookies!')
    with open(cookie_save_path, 'wb') as cookie_file:
        pickle.dump(session.cookies, cookie_file)
